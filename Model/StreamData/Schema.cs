﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.StreamData
{
    public class Schema
    {
        public string Type { get; set; }
        public object Fields { get; set; }
        public bool Optional { get; set; }
        public string Name { get; set; }
    }
}
