﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.StreamData
{
    public class MessageStream<T> : StreamBase
    {
        public PayLoadMessage<T> Payload { get; set; }
    }
}
