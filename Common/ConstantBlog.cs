﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public class ConstantBlog
    {
        public struct Operation
        {
            public const string Create = "c";
            public const string Update = "u";
            public const string Delete = "d";
        }

        public struct TableName
        {
            public const string Categories = "Categories";
            public const string CategoryLanguages = "CategoryLanguages";
        }

        public struct IndexName
        {
            public const string Category = "blog.category";
            public const string CategoryLanguage = "blog.categorylanguage";
        }
    }
}
