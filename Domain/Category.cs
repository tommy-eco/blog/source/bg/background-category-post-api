﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class Category
    {
        public Guid Id { get; set; }
        public IEnumerable<CategoryLanguage> CategoryLanguages { get; set; }
    }
}
