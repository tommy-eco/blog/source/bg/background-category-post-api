﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class CategoryLanguage
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LangCode { get; set; }
        public Guid CategoryId { get; set; }
    }
}
