﻿using Application.BaseApp.Command;
using Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.CategoryApp.Queries.Detail
{
    public class GetCategoryDetailQuery : IRequest<Category>
    {
        public Guid Id { get; set; }
    }
}
