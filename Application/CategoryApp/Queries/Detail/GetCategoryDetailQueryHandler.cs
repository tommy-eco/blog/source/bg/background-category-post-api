﻿using Domain;
using MediatR;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.CategoryApp.Queries.Detail
{
    public class GetCategoryDetailQueryHandler : IRequestHandler<GetCategoryDetailQuery, Category>
    {
        private readonly ICategoryRepository _categoryRepository;
        public GetCategoryDetailQueryHandler(ICategoryRepository categoryRepository)
        {
            this._categoryRepository = categoryRepository;
        }
        public async Task<Category> Handle(GetCategoryDetailQuery request, CancellationToken cancellationToken)
        {
            var category = await this._categoryRepository.GetById(request.Id);
            return category;
        }
    }
}
