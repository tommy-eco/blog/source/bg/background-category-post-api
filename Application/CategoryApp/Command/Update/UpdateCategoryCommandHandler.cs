﻿using Common;
using Domain;
using Mapster;
using MediatR;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.CategoryApp.Command.Update
{
    public class UpdateCategoryCommandHandler : IRequestHandler<UpdateCategoryCommand, bool>
    {
        private readonly ICategoryRepository _categoryRepository;
        public UpdateCategoryCommandHandler(ICategoryRepository categoryRepository)
        {
            this._categoryRepository = categoryRepository;
        }
        public async Task<bool> Handle(UpdateCategoryCommand request, CancellationToken cancellationToken)
        {
            var category = request.Adapt<Category>();
            if (category.Id != null)
            {
                string indexName = ConstantBlog.IndexName.Category;
                var response = await this._categoryRepository.UpdateDocumentAsync(category, indexName);
                return response.Success;
            }

            return false;
        }
    }
}
