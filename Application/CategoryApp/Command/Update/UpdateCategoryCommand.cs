﻿using Application.BaseApp.Command;
using Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.CategoryApp.Command.Update
{
    public class UpdateCategoryCommand : BaseCategoryCommand, IRequest<bool>
    {
        public IEnumerable<CategoryLanguage> CategoryLanguages { get; set; }
    }
}
