﻿using Application.BaseApp.Command;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.CategoryApp.Command.Create
{
    public class CreateCategoryCommand : BaseCategoryCommand, IRequest<bool>
    {
    }
}
