﻿using MediatR;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Domain;
using System.Threading.Tasks;
using Mapster;
using Common;
using Microsoft.Extensions.Logging;

namespace Application.CategoryApp.Command.Create
{
    public class CreateCategoryCommandHandler : IRequestHandler<CreateCategoryCommand, bool>
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly ILogger<CreateCategoryCommandHandler> _logger;
        public CreateCategoryCommandHandler(ILogger<CreateCategoryCommandHandler> logger, ICategoryRepository categoryRepository)
        {
            this._categoryRepository = categoryRepository;
            this._logger = logger;
        }
        public async Task<bool> Handle(CreateCategoryCommand request, CancellationToken cancellationToken)
        {
            var category = request.Adapt<Category>();
            if(category.Id != null)
            {
                string indexName = ConstantBlog.IndexName.Category;
                var response = await this._categoryRepository.CreateDocumentAsync(category, indexName);
                if (!response.Success)
                {
                    _logger.LogInformation(response.DebugInformation);
                }
                return response.Success;
            }

            return false;
        }
    }
}
