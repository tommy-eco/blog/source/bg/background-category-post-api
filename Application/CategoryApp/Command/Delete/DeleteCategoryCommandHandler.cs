﻿using Common;
using Domain;
using Mapster;
using MediatR;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.CategoryApp.Command.Delete
{
    public class DeleteCategoryCommandHandler : IRequestHandler<DeleteCategoryCommand, bool>
    {
        private readonly ICategoryRepository _categoryRepository;
        public DeleteCategoryCommandHandler(ICategoryRepository categoryRepository)
        {
            this._categoryRepository = categoryRepository;
        }
        public async Task<bool> Handle(DeleteCategoryCommand request, CancellationToken cancellationToken)
        {
            if (request.Id != null)
            {
                string indexName = ConstantBlog.IndexName.Category;
                var response = await this._categoryRepository.DeleteDocumentAsync(request.Id, indexName);
                return response.Success;
            }

            return false;
        }
    }
}
