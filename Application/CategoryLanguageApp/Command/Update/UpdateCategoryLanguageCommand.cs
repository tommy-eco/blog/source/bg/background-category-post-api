﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.CategoryLanguageApp.Command.Update
{
    public class UpdateCategoryLanguageCommand : IRequest<bool>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LangCode { get; set; }
        public Guid CategoryId { get; set; }
    }
}
