﻿using Common;
using Domain;
using Mapster;
using MediatR;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.CategoryLanguageApp.Command.Update
{
    public class UpdateCategoryLanguageCommandHandler : IRequestHandler<UpdateCategoryLanguageCommand, bool>
    {
        private readonly ICategoryLanguageRepository _categoryLanguageRepository;
        private readonly ILogger<UpdateCategoryLanguageCommandHandler> _logger;
        public UpdateCategoryLanguageCommandHandler(ICategoryLanguageRepository categoryLanguageRepository, ILogger<UpdateCategoryLanguageCommandHandler> logger)
        {
            this._categoryLanguageRepository = categoryLanguageRepository;
            this._logger = logger;
        }
        public async Task<bool> Handle(UpdateCategoryLanguageCommand request, CancellationToken cancellationToken)
        {
            var categoryLanguage = request.Adapt<CategoryLanguage>();
            if (categoryLanguage.Id != null)
            {
                string indexName = ConstantBlog.IndexName.CategoryLanguage;
                var response = await this._categoryLanguageRepository.UpdateDocumentAsync(categoryLanguage, indexName);
                return response.Success;
            }

            return false;
        }
    }
}
