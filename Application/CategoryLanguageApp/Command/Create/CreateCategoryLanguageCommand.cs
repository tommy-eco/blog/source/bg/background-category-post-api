﻿using Application.BaseApp.Command;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.CategoryLanguageApp.Command.Create
{
    public class CreateCategoryLanguageCommand : BaseCategoryLanguageCommand, IRequest<bool>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string LangCode { get; set; }
        public Guid CategoryId { get; set; }
    }
}
