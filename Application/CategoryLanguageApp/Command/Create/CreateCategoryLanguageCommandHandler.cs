﻿using Common;
using Domain;
using Mapster;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.CategoryLanguageApp.Command.Create
{
    public class CreateCategoryLanguageCommandHandler : IRequestHandler<CreateCategoryLanguageCommand, bool>
    {
        private readonly ICategoryLanguageRepository _categoryLanguageRepository;
        private readonly ILogger<CreateCategoryLanguageCommandHandler> _logger;
        public CreateCategoryLanguageCommandHandler(ILogger<CreateCategoryLanguageCommandHandler> logger, ICategoryLanguageRepository categoryLanguageRepository)
        {
            this._categoryLanguageRepository = categoryLanguageRepository;
            this._logger = logger;
        }
        public async Task<bool> Handle(CreateCategoryLanguageCommand request, CancellationToken cancellationToken)
        {
            var categoryLanguage = request.Adapt<CategoryLanguage>();
            if (categoryLanguage.Id != null)
            {
                string indexName = ConstantBlog.IndexName.CategoryLanguage;
                var response = await this._categoryLanguageRepository.CreateDocumentAsync(categoryLanguage, indexName);
                if(!response.Success)
                {
                    _logger.LogInformation(response.DebugInformation);
                }

                return response.Success;
            }

            return false;
        }
    }
}
