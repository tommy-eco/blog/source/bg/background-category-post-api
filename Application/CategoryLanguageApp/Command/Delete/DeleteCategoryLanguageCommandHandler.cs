﻿using Common;
using MediatR;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.CategoryLanguageApp.Command.Delete
{
    public class DeleteCategoryLanguageCommandHandler : IRequestHandler<DeleteCategoryLanguageCommand, bool>
    {
        private readonly ICategoryLanguageRepository _categoryLanguageRepository;
        private readonly ILogger<DeleteCategoryLanguageCommandHandler> _logger;
        public DeleteCategoryLanguageCommandHandler(ICategoryLanguageRepository categoryLanguageRepository, ILogger<DeleteCategoryLanguageCommandHandler> logger)
        {
            this._categoryLanguageRepository = categoryLanguageRepository;
            this._logger = logger;
        }
        public async Task<bool> Handle(DeleteCategoryLanguageCommand request, CancellationToken cancellationToken)
        {
            if (request.Id != null)
            {
                string indexName = ConstantBlog.IndexName.CategoryLanguage;
                var response = await this._categoryLanguageRepository.DeleteDocumentAsync(request.Id, indexName);
                if (!response.Success)
                {
                    _logger.LogInformation(response.DebugInformation);
                }

                return response.Success;
            }

            return false;
        }
    }
}
