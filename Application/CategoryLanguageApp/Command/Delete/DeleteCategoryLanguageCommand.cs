﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.CategoryLanguageApp.Command.Delete
{
    public class DeleteCategoryLanguageCommand : IRequest<bool>
    {
        public Guid Id { get; set; }
    }
}
