﻿using Common;
using Domain;
using MediatR;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.CategoryLanguageApp.Queries.Detail
{
    public class GetCategoryLanguageDetailQueryHandler : IRequestHandler<GetCategoryLanguageDetailQuery, CategoryLanguage>
    {
        private readonly ICategoryLanguageRepository _categoryLanguageRepository;
        public GetCategoryLanguageDetailQueryHandler(ICategoryLanguageRepository categoryLanguageRepository)
        {
            this._categoryLanguageRepository = categoryLanguageRepository;
        }
        public async Task<CategoryLanguage> Handle(GetCategoryLanguageDetailQuery request, CancellationToken cancellationToken)
        {
            var indexName = ConstantBlog.IndexName.CategoryLanguage;
            var category = await this._categoryLanguageRepository.GetById(request.Id, indexName);
            return category;
        }
    }
}
