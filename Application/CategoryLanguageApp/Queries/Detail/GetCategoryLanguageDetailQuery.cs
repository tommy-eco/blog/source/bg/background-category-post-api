﻿using Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.CategoryLanguageApp.Queries.Detail
{
    public class GetCategoryLanguageDetailQuery : IRequest<CategoryLanguage>
    {
        public Guid Id { get; set; }
    }
}
