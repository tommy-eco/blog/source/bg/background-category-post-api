using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Repository.Implement;
using Repository.Interfaces;
using Services.Implement;
using Services.Interfaces;

namespace Worker
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    IConfiguration configuration = hostContext.Configuration;
                    services.ConnectElasticSearch(configuration);

                    // Add DI
                    // services
                    services.AddScoped<ICategoryService, CategoryService>();
                    services.AddScoped<ICategoryLanguageService, CategoryLanguageService>();


                    services.AddScoped<ICategoryRepository, CategoryRepository>();
                    services.AddScoped<ICategoryLanguageRepository, CategoryLanguageRepository>();

                    // add mediator
                    var assembly = AppDomain.CurrentDomain.Load("Application");
                    services.AddMediatR(assembly);

                    services.AddHostedService<CatePostWorker>();
                });
    }
}
