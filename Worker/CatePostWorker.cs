using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Confluent.Kafka;
using Elasticsearch.Net;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Model;
using Model.StreamData;
using Nest;
using Newtonsoft.Json;
using Services.Interfaces;

namespace Worker
{
    public class CatePostWorker : BackgroundService
    {
        private readonly ILogger<CatePostWorker> _logger;
        private IServiceScopeFactory _serviceScopeFactory;
        private readonly ICategoryService _categoryService;
        private readonly ICategoryLanguageService _categoryLanguageService;

        public CatePostWorker(ILogger<CatePostWorker> logger, IServiceScopeFactory serviceScopeFactory)
        {
            _logger = logger;
            this._serviceScopeFactory = serviceScopeFactory;
            this._categoryService = this._serviceScopeFactory.CreateScope().ServiceProvider.GetService<ICategoryService>();
            this._categoryLanguageService = this._serviceScopeFactory.CreateScope().ServiceProvider.GetService<ICategoryLanguageService>();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // kafka
            var config = new ConsumerConfig()
            {
                BootstrapServers = "192.168.50.8:9092",
                GroupId = "blog-category-post",
                AutoOffsetReset = AutoOffsetReset.Earliest,
                EnableAutoCommit = false
            };
            var listTopic = new List<string> { "TOMMY.dbo.Categories", "TOMMY.dbo.CategoryLanguages" };

            using (var consumer = new ConsumerBuilder<string, string>(config).Build())
            {
                consumer.Subscribe(listTopic);
                while (!stoppingToken.IsCancellationRequested)
                {
                    _logger.LogInformation("Waiting Message --- {time}", DateTimeOffset.Now);
                    await Task.Factory.StartNew(async () =>
                    {
                        await this.ExcuteConsume(consumer, stoppingToken);
                    });
                }
            }
        }


        protected async Task ExcuteConsume(IConsumer<string, string> consumer, CancellationToken stoppingToken)
        {
            try
            {
                var consumeResult = consumer.Consume(stoppingToken);
                _logger.LogInformation("Key: {0}", consumeResult.Message.Key);
                _logger.LogInformation("Message: {0}", consumeResult.Message.Value);
                if (consumeResult.Message != null)
                {
                    // parse key
                    var keyParse = JsonConvert.DeserializeObject<KeyStream>(consumeResult.Message.Key);
                    var keyNameDbTopic = keyParse.Schema.Name.Split('.');
                    var tableName = keyNameDbTopic.Count() > 1 ? keyNameDbTopic[2].Trim() : "";
                    var response = false;
                    if (!string.IsNullOrEmpty(tableName))
                    {
                        switch (tableName)
                        {
                            case ConstantBlog.TableName.Categories:
                                _logger.LogInformation("Table Name: {0}", tableName);
                                // Deserialize Object to Category
                                response = await this._categoryService.ProcessingMessage(consumeResult.Message.Value);
                                break;
                            case ConstantBlog.TableName.CategoryLanguages:
                                _logger.LogInformation("Table Name: {0}", tableName);
                                // Deserialize Object to CategoryLanguage
                                response = await this._categoryLanguageService.ProcessingMessage(consumeResult.Message.Value);
                                break;
                            default:
                                response = false;
                                break;
                        }
                    }

                    // message
                    if (response)
                    {
                        consumer.Commit(consumeResult);
                        _logger.LogInformation("Processing Message Success for table {0}", tableName);
                    }
                    else
                    {
                        _logger.LogError("Processing Message Failed for table {0}", tableName);
                    }
                }
                else
                {
                    // commit message when message is null -> delete
                    consumer.Commit(consumeResult);
                }

            }
            catch(Exception ex)
            {
                _logger.LogInformation($"Failed Cause {ex.Message}");
            }
           
        }
    }
}
