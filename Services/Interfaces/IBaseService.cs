﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IBaseService<T>
    {
        Task<bool> Create(T item);
        Task<bool> Update(T item);
        Task<bool> Delete(T item);
        Task<bool> ProcessingMessage(string message);
    }
}
