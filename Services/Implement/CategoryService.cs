﻿using Application.CategoryApp.Command.Create;
using Application.CategoryApp.Command.Delete;
using Application.CategoryApp.Command.Update;
using Common;
using Domain;
using Elasticsearch.Net;
using MediatR;
using Microsoft.Extensions.Logging;
using Model.StreamData;
using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implement
{
    public class CategoryService : ICategoryService
    {
        private readonly IMediator _mediator;
        private readonly ILogger<CategoryService> _logger;
        public CategoryService(IMediator mediator, ILogger<CategoryService> logger)
        {
            this._mediator = mediator;
            this._logger = logger;
        }

        public async Task<bool> Create(Category item)
        {
            var categoryCommand = new CreateCategoryCommand
            {
                Id = item.Id
            };
            var response = await this._mediator.Send(categoryCommand);
            return response;
        }

        public async Task<bool> Delete(Category item)
        {
            var categoryCommand = new DeleteCategoryCommand
            {
                Id = item.Id
            };
            var response = await this._mediator.Send(categoryCommand);
            return response;
        }

        public async Task<bool> ProcessingMessage(string message)
        {
            bool response = false;
            string action = string.Empty;
            if (!string.IsNullOrEmpty(message))
            {
                var messageParse = JsonConvert.DeserializeObject<MessageStream<Category>>(message);
                if (messageParse.Payload != null)
                {
                    var consumeParse = messageParse.Payload;
                    switch (consumeParse.Op)
                    {
                        case ConstantBlog.Operation.Delete:
                            action = "Delete";
                            _logger.LogInformation($"Operation: {action} Category Language Id: {consumeParse.Before.Id}");
                            response = await this.Delete(consumeParse.Before);
                            break;
                        case ConstantBlog.Operation.Update:
                            action = "Update";
                            _logger.LogInformation($"Operation: {action} Category Language Id: {consumeParse.After.Id}");
                            response = await this.Update(consumeParse.After);
                            break;
                        case ConstantBlog.Operation.Create:
                            action = "Create";
                            _logger.LogInformation($"Operation: {action} Category Language Id: {consumeParse.After.Id}");
                            response = await this.Create(consumeParse.After);
                            break;
                        default:
                            response = false;
                            _logger.LogInformation($"Not Dectect Any Action");
                            break;
                    }
                } 
            }

            return response;
        }

        public async Task<bool> Update(Category item)
        {
            var categoryCommand = new UpdateCategoryCommand
            {
                Id = item.Id
            };
            var response = await this._mediator.Send(categoryCommand);
            return response;
        }
    }
}
