﻿using Application.CategoryApp.Command.Update;
using Application.CategoryApp.Queries.Detail;
using Application.CategoryLanguageApp.Command.Create;
using Application.CategoryLanguageApp.Command.Delete;
using Application.CategoryLanguageApp.Command.Update;
using Application.CategoryLanguageApp.Queries.Detail;
using Common;
using Domain;
using Mapster;
using MediatR;
using Microsoft.Extensions.Logging;
using Model.StreamData;
using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implement
{
    public class CategoryLanguageService : ICategoryLanguageService
    {
        private readonly IMediator _mediator;
        private readonly ILogger<CategoryLanguageService> _logger;
        public CategoryLanguageService(IMediator mediator, ILogger<CategoryLanguageService> logger)
        {
            this._mediator = mediator;
            this._logger = logger;
        }

        public async Task<bool> Create(CategoryLanguage item)
        {
            // index category language 
            var categoryLanguageCommand = item.Adapt<CreateCategoryLanguageCommand>();
            await this._mediator.Send(categoryLanguageCommand);

            // update category
            var getCategoryQuery = new GetCategoryDetailQuery
            {
                Id = item.CategoryId
            };

            var category = await this._mediator.Send(getCategoryQuery);
            _logger.LogWarning($"Category get from Elasticsearch: {JsonConvert.SerializeObject(category)}");
            if (category != null)
            {
                var listCategoryLanguage = new List<CategoryLanguage>();
                if (category.CategoryLanguages != null)
                {
                    if(category.CategoryLanguages.Any())
                    {
                        listCategoryLanguage = category.CategoryLanguages.ToList();
                    }
                }
                else
                {
                    category.CategoryLanguages = listCategoryLanguage;
                }
                listCategoryLanguage.Add(item);
                category.CategoryLanguages = listCategoryLanguage;

                // update category
                var categoryCommand = category.Adapt<UpdateCategoryCommand>();
                _logger.LogWarning($"CategoryCommand update object Elasticsearch: {JsonConvert.SerializeObject(categoryCommand)}");
                var response = await this._mediator.Send(categoryCommand);
                return response;
            }
            return false;

        }

        public async Task<bool> Delete(CategoryLanguage item)
        {
            // delete category language
            var categoryLanguageCommand = new DeleteCategoryLanguageCommand
            {
                Id = item.Id
            };

            await this._mediator.Send(categoryLanguageCommand);

            // get category
            var getCategoryQuery = new GetCategoryDetailQuery
            {
                Id = item.CategoryId
            };

            // update category language
            var category = await this._mediator.Send(getCategoryQuery);
            _logger.LogWarning($"Category get from Elasticsearch: {JsonConvert.SerializeObject(category)}");
            if (category != null)
            {
                if(category.CategoryLanguages != null)
                {
                    if(category.CategoryLanguages.Any())
                    {
                        var listCategoryLanguage = category.CategoryLanguages.ToList();
                        listCategoryLanguage.Remove(item);
                        category.CategoryLanguages = listCategoryLanguage;
                    }
                }

                var categoryCommand = category.Adapt<UpdateCategoryCommand>();
                _logger.LogWarning($"CategoryCommand update object Elasticsearch: {JsonConvert.SerializeObject(categoryCommand)}");
                var response = await this._mediator.Send(categoryCommand);
                return response;
            }

            return false;
        }

        public async Task<bool> Update(CategoryLanguage item)
        {
            // update category
            var categoryLanguageCommand = item.Adapt<UpdateCategoryLanguageCommand>();
            await this._mediator.Send(categoryLanguageCommand);

            // get category
            var getCategoryQuery = new GetCategoryDetailQuery
            {
                Id = item.CategoryId
            };

            var category = await this._mediator.Send(getCategoryQuery);
            _logger.LogWarning($"Category get from Elasticsearch: {JsonConvert.SerializeObject(category)}");
            if (category != null)
            {
                if (category.CategoryLanguages != null)
                {
                    if (category.CategoryLanguages.Any())
                    {
                        var categoryLanguage = category.CategoryLanguages.FirstOrDefault(c => c.Id == item.Id);
                        // update
                        if(categoryLanguage != null)
                        {
                            categoryLanguage.Description = item.Description;
                            categoryLanguage.Name = item.Name;
                        }
                    }
                }

                var categoryCommand = category.Adapt<UpdateCategoryCommand>();
                _logger.LogWarning($"CategoryCommand update object Elasticsearch: {JsonConvert.SerializeObject(categoryCommand)}");
                var response = await this._mediator.Send(categoryCommand);
                return response;
            }

            return false;
        }

        public async Task<bool> ProcessingMessage(string message)
        {
            bool response = false;
            string action = string.Empty;
            if (!string.IsNullOrEmpty(message))
            {
                var messageParse = JsonConvert.DeserializeObject<MessageStream<CategoryLanguage>>(message);
                if (messageParse.Payload != null)
                {
                    var consumeParse = messageParse.Payload;
                    switch (consumeParse.Op)
                    {
                        case ConstantBlog.Operation.Delete:
                            action = "Delete";
                            _logger.LogInformation($"Operation: {action} Category Language Id: {consumeParse.Before.Id}");
                            response = await this.Delete(consumeParse.Before);
                            break;
                        case ConstantBlog.Operation.Update:
                            action = "Update";
                            _logger.LogInformation($"Operation: {action} Category Language Id: {consumeParse.After.Id}");
                            response = await this.Update(consumeParse.After);
                            break;
                        case ConstantBlog.Operation.Create:
                            action = "Create";
                            _logger.LogInformation($"Operation: {action} Category Language Id: {consumeParse.After.Id}");
                            response = await this.Create(consumeParse.After);
                            break;
                        default:
                            response = false;
                            _logger.LogError($"Not Dectect Any Action");
                            break;
                    }
                } 
            }

            return response;
        }
    }
}
