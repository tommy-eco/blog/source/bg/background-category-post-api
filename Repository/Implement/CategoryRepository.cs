﻿using Domain;
using Elasticsearch.Net;
using Nest;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Implement
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly IElasticClient _client;
        public CategoryRepository(IElasticClient client)
        {
            this._client = client;
        }
        public async Task<IApiCallDetails> CreateDocumentAsync(Category document, string indexName = "")
        {
            if (document != null)
            {
                indexName = !string.IsNullOrEmpty(indexName) ? indexName : this._client.ConnectionSettings.DefaultIndex;
                var response = await this._client.IndexAsync(document,  index => index.Index(indexName));
                return response.ApiCall;
            }
            return null;
        }

        public async Task<IApiCallDetails> DeleteDocumentAsync(Guid id, string indexName = "")
        {
            if (id != null)
            {
                indexName = !string.IsNullOrEmpty(indexName) ? indexName : this._client.ConnectionSettings.DefaultIndex;
                var response = await this._client.DeleteAsync(DocumentPath<Category>.Id(id), d => d.Index(indexName));
                return response.ApiCall;
            }
            return null;
        }

        public async Task<Category> GetById(Guid id, string indexName = "")
        {
            if (id != null)
            {
                indexName = !string.IsNullOrEmpty(indexName) ? indexName : this._client.ConnectionSettings.DefaultIndex;
                var response = await this._client.GetAsync<Category>(DocumentPath<Category>.Id(id), g => g.Index(indexName));
                return response.Source;
            }
            return new Category();
        }

        public async Task<IApiCallDetails> UpdateDocumentAsync(Category document, string indexName = "")
        {
            if (document != null)
            {
                indexName = !string.IsNullOrEmpty(indexName) ? indexName : this._client.ConnectionSettings.DefaultIndex;
                var response = await this._client.UpdateAsync(
                    DocumentPath<Category>.Id(document.Id), u => 
                                                u.Doc(document)
                                                .Refresh(Refresh.WaitFor)
                                                .Index(indexName));
                return response.ApiCall;
            }
            return null;
        }
    }
}
