﻿using Domain;
using Elasticsearch.Net;
using Nest;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Implement
{
    public class CategoryLanguageRepository : ICategoryLanguageRepository
    {
        private readonly IElasticClient _client;
        public CategoryLanguageRepository(IElasticClient client)
        {
            this._client = client;
        }
        public async Task<IApiCallDetails> CreateDocumentAsync(CategoryLanguage document, string indexName = "")
        {
            if (document != null)
            {
                indexName = !string.IsNullOrEmpty(indexName) ? indexName : this._client.ConnectionSettings.DefaultIndex;
                var response = await this._client.IndexAsync(document, index => index.Index(indexName));
                return response.ApiCall;
            }
            return null;
        }

        public async Task<IApiCallDetails> DeleteDocumentAsync(Guid id, string indexName = "")
        {
            if (id != null)
            {
                indexName = !string.IsNullOrEmpty(indexName) ? indexName : this._client.ConnectionSettings.DefaultIndex;
                var response = await this._client.DeleteAsync(DocumentPath<CategoryLanguage>.Id(id), d => d.Index(indexName));
                return response.ApiCall;
            }
            return null;
        }

        public async Task<CategoryLanguage> GetById(Guid id, string indexName = "")
        {
            if(id != null)
            {
                indexName = !string.IsNullOrEmpty(indexName) ? indexName : this._client.ConnectionSettings.DefaultIndex;
                var response = await this._client.GetAsync<CategoryLanguage>(DocumentPath<CategoryLanguage>.Id(id), g => g.Index(indexName));
                return response.Source;
            }
            return new CategoryLanguage();
        }

        public async Task<IApiCallDetails> UpdateDocumentAsync(CategoryLanguage document, string indexName = "")
        {
            if (document != null)
            {
                indexName = !string.IsNullOrEmpty(indexName) ? indexName : this._client.ConnectionSettings.DefaultIndex;
                var response = await this._client.UpdateAsync(
                    DocumentPath<CategoryLanguage>.Id(document.Id), u =>
                                                u.Doc(document)
                                                .Refresh(Refresh.WaitFor)
                                                .Index(indexName));
                return response.ApiCall;
            }
            return null;
        }
    }
}
